package com.example.loveindex;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	EditText firstName, lastName;
	TextView output;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		firstName = (EditText) findViewById(R.id.firstname_edittext);
		lastName = (EditText) findViewById(R.id.secondname_edittext);
		Button button = (Button) findViewById(R.id.find_button);
		output = (TextView) findViewById(R.id.resultText);
		button.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		if (R.id.find_button == v.getId()) {
			findIndex();
		}
		
	}
	//LOGIC FOR FINDING LOVE INDEX
		private void findIndex() {
			if (!firstName.getText().toString().isEmpty()&&!lastName.getText().toString().isEmpty()) {
				
			 
				String fst = firstName.getText().toString().toLowerCase();
				String scnd = lastName.getText().toString().toLowerCase();
				double result = 0;

				StringBuffer str1 = new StringBuffer(fst);
				StringBuffer str2 = new StringBuffer(scnd);

				for (int i = 0; i < str1.length(); i++) {
					for (int j = 0; j < str2.length(); j++) {

						String d1 = String.valueOf(str1.charAt(i));
						String d2 = String.valueOf(str2.charAt(j));
						if (d1.equalsIgnoreCase(d2)) {
							str1.deleteCharAt(i);
							i = i - 1;
							str2.deleteCharAt(j);
							break;
						}
					}
				}
				int value = 0;
				for (int i = 0; i < str1.length(); i++) {

					value += (str1.charAt(i) - 96);
				}
				int value1 = 0;
				for (int i = 0; i < str2.length(); i++) {

					value1 += (str2.charAt(i) - 96);
				}
				double res1 = Math.min(value, value1);
				double res2 = Math.max(value, value1);
				result = (res1 / res2) * 100;

				output.setText(String.valueOf(Math.abs(result)));
			}else{
				Toast.makeText(getApplicationContext(),"Fields should not be empty",Toast.LENGTH_LONG).show();
			}

		}
}